---
title: SlimeVR
weight: 250
---

# SlimeVR

- [SlimeVR Documentation](https://docs.slimevr.dev/)

> SlimeVR is a set of open hardware sensors and open source software that facilitates full-body tracking (FBT) in virtual reality. 

SlimeVR is an open source IMU based full body tracking system. Being open source it has Linux support and there are a few variants you can either buy or build yourself.

You can pre-order the official SlimeVR trackers from [their crowdsupply page](https://www.crowdsupply.com/slimevr/slimevr-full-body-tracker), but at the time of writing this if you order them right now you'd need to wait approximately 4 months for your orders to ship, due to the very high demand driven by low cost and good quality tracking compared to other solutions.

## Building

Alternatively you can build it on your own, and you have a few variants to choose from:

- [Official SlimeVR DIY builders guide](https://docs.slimevr.dev/diy/index.html)
- [Frozen Slimes V2](https://github.com/frosty6742/frozen-slimes-v2)
  - while a bit bigger, Frozen Slimes V2 tend to be cheaper and a lot easier to build, being approachable even for people that are inexperienced with soldering

It's generally **highly recommended** to opt for the more expensive but higher quality [BNO085](https://shop.slimevr.dev/products/slimevr-imu-module-bno085) IMU module. This should offer the highest quality tracking with best precision and minimal drifting.

## SlimeVR app

<!-- will likely need to move this somewhere else -->

- [SlimeVR on Linux guide](https://docs.slimevr.dev/tools/linux-installation.html)
- [Flathub](https://flathub.org/apps/dev.slimevr.SlimeVR)
