---
title: WMR Lighthouse
weight: 9000
---

# Running WMR with Lighthouse on SteamVR

This is a guide on how to use WMR as a display source and lighthouse as tracking system for WMR on Linux to use with SteamVR.

It will not utilise space calibration techniques as at the moment of writing, Basalt on WMR is drifting, and will crash the second it looses tracking (which it might do frequently if left unattended).

Instead, it will override head/camera in steamvr to be used from tracker.

## Why?

Because WMR at the moment might be the only viable native pcvr headset that isn't index, costs dirt cheap and has the most features that Monado can support for later use. Overall if you have vive and want higher resolution while not buying something costly, buying WMR, vive tracker (or tundra) for such purpose might just be a good option.

Otherwise, it's just fun to see how far you can stretch definition of *jank vr setup* but actually kind of useful.

## Prerequisites

1. Working Monado with WMR (this guide is aimed towards using [Envision](/docs/fossvr/envision/) for that)
2. Working SteamVR lighthouse system
   - For head tracker, you can use any lighthouse equipment you have, it doesn't matter, but preferably it would be a small tracker, like a tundra one, so you can use it for more comfortable experience
   - You need to connect your tracker at leas once into SteamVR to be recognized in next steps for serial number
3. Getting serial number of your desired head tracker
   - You can get it from `~/.steam/steam/config/lighthouse/lighthousedb.json`. Inside it there would be `known_objects` section which contains all lighthouse devices. Find one that looks like correct device and copy it's `serialNumber`

## Envision

It's preferrable to create a separate WMR profile in Envision with the following changes compared to the default WMR profile:

- disable Basalt
- disable pull on build
- use a different prefix
- use a different Monado source directory (technically optional, but this way it won't mess with your main prefixes since we will commit into those source repositories)

## Monado

You would need to make sure that WMR headset is the only one being initialised in Monado. In my case it wasn't, because i used my Vive as dongles for my knuckles, so i had to comment out vive driver from Monado completely (there's probably a better way to do this rather than editing the Monado source directly, if you the reader found one, make sure to contribute it here).

For that (assuming default locations, adjust if necessary), navigate to your Monado source directory your Envision profile uses, the default would be: `~/.local/share/envision/monado`. Open CMakeLists.txt and:

1. Comment line starting with `option_with_deps(XRT_HAVE_BASALT...` (it will look like `#option_with_deps(XRT_HAVE_BASALT...` )
2. Comment (with `#`) 3 lines in final list of compiled drivers:
   1. `"SURVIVE"`
   2. `"VIVE"`
   3. `"STEAMVR_LIGHTHOUSE"`
3. `git add . && git commit -m "wmr_lh comments"`

And rebuild Monado, either manually or by using Envision.

## SteamVR

For starters, we need to register Monado as driver for SteamVR:

Assuming default installation for both Steam and Monado inside Envision, the command would be: `~/.steam/steam/steamapps/common/SteamVR/bin/vrpathreg.sh adddriver ~/.local/share/envision/prefixes/wmr_default/share/steamvr-monado`. Adjust the command if you have different paths for steam, prefixes.

Now we need to override the head to be assigned to our tracker

If you're using Vive as dongles for other controllers as I did, SteamVR will certainly try to load it first instead of Monado. This might also happen even when using separate dongles, so I would follow these next steps regardless.

Open `~/.steam/steam/config/steamvr.vrsettings` and add this:

```
"TrackingOverrides" : {
    "/devices/htc/vive_trackerLHR-SERIAL" : "/user/head"
},
"steamvr" : {
    "activateMultipleDrivers" : true,
    "forcedDriver" : "monado"
},
"trackers" : {
    "/devices/htc/vive_trackerLHR-SERIAL" : "TrackerRole_Camera"
}
```

In which you replace `LHR-SERIAL` with your own serial number that you got from previous steps.

`"steamvr"` section should be merged with yours and will allow to activate both lighthouse and monado driver (while monado being primary display). 

Be careful with commas in JSON: trailing commas are not allowed but commas are required between entries. If your JSON is invalid, SteamVR will reset it. It's also a good idea to keep backups.

## Post-process

Launch SteamVR as usual, and go along with room setup while both headset and tracker are on the ground and visible by base stations. This should recenter them and place onto ground as usual.

It might be possible also that games will try to use default monado controller for games, which might also break games in some ways sometimes, so be wary that this guide *might* not work for some games. Any additional help to resolve that is appreciated (I did not try to emulate Index knuckles from SteamVR Monado driver yet).

Otherwise, it should work now and track the headset using the position data from the tracker.

## Conclusion & Thoughts

At the time of writing this I tested it only with a Vive wand as tracker (see [Turning Vive Wands into Vive Trackers for FULL BODY VR - YouTube](https://www.youtube.com/watch?v=JZrDIvls1ms)), unfortunately requires Windows for flashing devices but it's most likely that you can do it through a virtual machine using USB passthrough (KVM/QEMU or libvirt virtual machines are preferrable for this, VirtualBox can cause irreparable issues when flashing devices via USB passthrough).

While it didn't work great (obviously, loosely strapping vive wand as head tracker on any headset isn't the most pleasant experience), it proved to be a viable way to experience native PCVR on WMR.

What most impressed me in this is colours on that wmr. On Windows, WMR looked very dull and unsaturated to the point i didn't want to use it at all, while there it looks quite vibrant and pretty nice, i actually want to use it now there.

**Used hardware**: Dell Visor VRP 100, Vive as dongles for Index knuckles, 1.0 base station

## Additional links

- How to turn tracker into head tracker - [Track Any VR Headset With a Vive Tracker! - YouTube](https://www.youtube.com/watch?v=IgHUqcVUyQA)
