---
weight: 100
title: SteamVR
---

# SteamVR

The de facto standard for anything PCVR, SteamVR runs on Linux.

Unfortunately the Linux version is riddled with bugs, missing features and bad performace, so in general it can be a subpar experience.

You can use one of the alternatives listed in the [FOSS VR section](/docs/fossvr/).

If you want to use a standalone headset with SteamVR you can check out [ALVR](/docs/steamvr/alvr/).

## Room Setup

The SteamVR room setup application is bug prone. It's possible to circumvent the room setup application using one of the following methods.

### Quick Calibration

Set the headset and controllers on the floor in the center of the playspace. Run the Quick Calibration utility under _Settings > Developer > Room and Tracking_. This will setup a standing play area for you to use.

### Envision

[Envision](/docs/fossvr/envision/) can run a quick calibration with the press of a button. This is convenient if you want to use [Monado](/docs/fossvr/monado/) instead of SteamVR, but the calibration will also work on plain SteamVR.

### VRCMD

For a (nearly) headless option to do the room setup. First, turn on the basestations and controllers. Put the headset and controllers in the center of your play space.

Next, open two terminal windows with `/home/user/.local/share/Steam/steamapps/common/SteamVR/bin/linux64` as the working directory. In one window, run the following command. This command will start the SteamVR server and generate pose data.

```bash
LD_LIBRARY_PATH=$(pwd) ./vrcmd --pollposes
```

In the second terminal window, run the following command. This command will perform a similar function to the [Quick Calibration](#quick-calibration) method described above.

```bash
LD_LIBRARY_PATH=$(pwd) ./vrcmd --resetroomsetup
```

Use `Ctrl+C` in the first terminal window to stop the SteamVR server. 
