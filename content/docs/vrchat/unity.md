---
weight: 900
title: Unity
---

# Unity

For the best experience, we found that running Unity from [Flatpak](https://flathub.org/apps/com.unity.UnityHub) is usually preferred.

## Setting up a VRChat avatar/world project

First, take a look at this page from [the official VRChat docs](https://creators.vrchat.com/sdk/upgrade/unity-2022/#using-the-unity-website) about the Unity 2022 upgrade. We'll do something similar.

1. Install Unity Hub from Flatpak.
1. Install the 3rd party VRChat package manager [`vrc-get`](https://github.com/anatawa12/vrc-get).
   - **NOTE**: while the version you can install via `cargo` works fine, it is actually out of date, so a better option would be to build from source and add the executable to your `$PATH`.
1. Open Unity Hub and sign in with your Unity account.
1. Install **2022.3.6f1** (the current supported Unity version, at the time of writing) by running `xdg-open unityhub://2022.3.6f1/b9e6e7e9fa2d`.
1. When prompted, select *Android Build Support* and *Windows Build Support (Mono)*.
1. Create a new project with the **3D Core template**, making sure to name it and give it a better location than `$HOME`.
1. If it opens a window partially offscreen like it did for me (KWin Wayland), press ALT+F3 -> More Actions -> Move.
1. Close the Unity project (will probably crash later if you don't, no big deal but still).
1. Open terminal and `cd` to the project dir.
1. Run either `vrc-get install com.vrchat.avatars` or `vrc-get install com.vrchat.worlds` depending on what you will be making.
1. Reopen the Unity project and it will import the new packages.
1. Go to VRChat SDK -> Show Control Panel and dock it to the main window.

Now you should be able to create and upload avatars and worlds like you normally would.

If you're having issues, consult the [Troubleshooting](#troubleshooting) section.


## Troubleshooting

### NullReferenceException when uploading

This is an SDK bug. [Vote for this Canny!](https://feedback.vrchat.com/sdk-bug-reports/p/350-beta1-avatar-upload-fails-on-linux-editor)

To patch it temporarily, run this command:

```bash
cd ~/YourProject
git apply << EOF
diff --git a/Packages/com.vrchat.base/Editor/VRCSDK/Dependencies/VRChat/API/VRCTools.cs b/Packages/com.vrchat.base/Editor/VRCSDK/Dependencies/VRChat/API/VRCTools.cs
index 5104edb..5820d61 100644
--- a/Packages/com.vrchat.base/Editor/VRCSDK/Dependencies/VRChat/API/VRCTools.cs
+++ b/Packages/com.vrchat.base/Editor/VRCSDK/Dependencies/VRChat/API/VRCTools.cs
@@ -108,7 +108,13 @@ namespace VRC.SDKBase.Editor.Api
                 {
                     continue;
                 }
-                var groups = (IEnumerable)groupsList?.GetValue(servicePointGroups.GetValue(scheduler));
+                // var groups = (IEnumerable)groupsList?.GetValue(servicePointGroups.GetValue(scheduler));
+                var servicePointGroup = servicePointGroups?.GetValue(scheduler);
+                if (servicePointGroup == null)
+                {
+                    continue;
+                }
+                var groups = (IEnumerable)groupsList?.GetValue(servicePointGroup);
 
                 // we're going to retry finding the active service point
                 if (groups == null)
EOF
```

## Using VCC instead?

The official VRChat Creator Companion uses Webview2 (Microsoft Edge). This component is difficult to install. Help wanted.
