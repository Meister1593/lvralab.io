---
weight: 100
title: Video Players
---

# Video Players

AVPro based video players are notoriously buggy on Linux, causing issues such as high memory usage, crashing or just straight out not working. 

We recommend using [GE-Proton](https://github.com/GloriousEggroll/proton-ge-custom), as it contains extra fixes for Wine's Windows Media Foundation implementation that AVPro uses. This will allow you to play regular videos, MPEG-TS and HLS streams, but not RTSP streams.

{{< hint info >}}

The overwhelming majority of live music events utilize RTSP for its low latency which leaves proton users in the dark. Ask your venue for stream links if none are provided at the stage and view the stream in the background with your web browser or hover it in space with either [WlxOverlay](/docs/steamvr/wlxoverlay/) or [WlxOverlay-X](/docs/fossvr/wlxoverlay-x/).

{{< /hint >}}

AVPro on GE is currently expected to be stable, with one excpetion of seeking in the video not currently being implemented, if you encounter game stability issues or other around video playback please report them as soon as possible at the [VRChat proton git issue](https://github.com/ValveSoftware/Proton/issues/1199) or our [community instant messaging](/docs/community/).

## Stream from video player to ffplay/MPV

When in a VRChat world, if a video player fails to play, you can instead run [this script](https://gist.github.com/galister/1a971254af72bb2a5bc27740e984bce2).

You might need to adapt it in case you installed VRChat in another disk and not in your home folder.

It essentially grabs the URL from the VRChat logs and opens it in ffplay (for RTMP streams) or MPV.
